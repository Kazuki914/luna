# Project Name : Luna

## How To  

1. clone:  
`git clone git@bitbucket.org:Kazuki914/luna.git`  
2. install required modules:  
`pip install -r requirements.txt`  
3. migrate models:  
`$ python manage.py makemigrations`  
`$ python manage.py migrate`  
4. run server:  
`python manage.py runserver`  

## NEED
1. Python 3
2. Django* (pip install django)  
