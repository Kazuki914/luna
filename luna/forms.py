from django import forms

class LunaForm(forms.Form):
    year = forms.IntegerField(
        label='Year',
        required=True,
    )
    month = forms.IntegerField(
        label='Month',
        required=True,
    )
    date = forms.IntegerField(
        label='Date',
        required=True,
    )
