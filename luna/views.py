from django.shortcuts import render, redirect, get_object_or_404

from django.views.generic import TemplateView
from luna.models import *

from .forms import LunaForm

from .modules import lunaage

def LunaTopView(request):
    form = LunaForm(request.POST or None)
    ages = luna.objects.all()
    return render(request, 'index.html', {'form': form, 'ages': ages})

def ResultView(request):
    if request.method == 'POST':
        form = LunaForm(request.POST)
        lunamodel = luna()
        if not form.is_valid():
            raise ValueError('invalid form')
        year = form.cleaned_data['year']
        month = form.cleaned_data['month']
        day = form.cleaned_data['date']
        hour=12
        min=0
        sec=0
        lunar_date = lunaage.lunar_date_calc(year, month, day, hour, min, sec)
        luna.year = year
        luna.month = month
        luna.day = day
        luna.age = round(lunar_date, 1)
        luna.objects.create(
            year = luna.year,
            month = luna.month,
            day = luna.day,
            age = luna.age,
        )
    return render(request, 'result.html', {'lunar_date' : lunar_date, 'year':year, 'month': month, 'day': day})
