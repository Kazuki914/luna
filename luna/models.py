from django.db import models

# Create your models here.


class luna(models.Model):
    year = models.IntegerField()
    month = models.IntegerField()
    day = models.IntegerField()
    age = models.DecimalField(
            max_digits=4,
            decimal_places=1,
        )
